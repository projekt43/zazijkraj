/*
// Funkce pro aktualizaci šířky a posunutí prvku s třídou "swiper"
function aktualizovatSirkuAPosunuti() {
  const sirkaZarizeni = window.innerWidth;
  const swiperElement = document.querySelector(".swiper");
  const posunuti = (sirkaZarizeni - swiperElement.offsetWidth) / 2;
  swiperElement.style.width = sirkaZarizeni + 'px';
  swiperElement.style.right = posunuti + 'px';
}

// Zavolání funkce při načtení stránky a po každé změně velikosti okna prohlížeče
window.addEventListener("load", aktualizovatSirkuAPosunuti);
window.addEventListener("resize", aktualizovatSirkuAPosunuti);
*/
/*
// získání výšky okna
const windowHeight = window.innerHeight;
// získání prvku s třídou header
const header = document.querySelector('.header');
// získání výšky prvku header
const headerHeight = header.offsetHeight;
// výpočet výšky body (odečtení výšky headeru od výšky okna)
const bodyHeight = windowHeight - headerHeight;
// nastavení výšky prvku body na výslednou hodnotu
document.querySelector('body').style.height = bodyHeight + 'px';

const section3width = document.querySelector('.section3');
console.log(section3)
*/

$(document).ready(function(){

const aplikace_slider = new Swiper('.aplikace_slider', {
  
  loop: true,
  pagination: {
    el: '.swiper-pagination',
  },
  breakpoints: {
    0: {
      slidesPerView: 1,
      spaceBetween: 20,
    },
  },
});


$('.controls .next').click(function(){
  aplikace_slider.slideNext();
})

$('.controls .prev').click(function(){
  aplikace_slider.slidePrev();
})

})

/*
***                                                     spoluprace new_text
*/

$(document).ready(function() {
  if ($(window).width() < 768) {
    $('#newText,#newText2').remove();
    
  }
});
/*
***                                                     spoluprace SECTION 2
*/

const newTextElement = document.getElementById("newText");
const words = [ "hotel?","restaurace?", "rozhledna?", "zajímavá turistická lokalita?"];
let currentWordIndex = 0;
let currentLetterIndex = 0;
let isDeleting = false;

function typeNextLetter() {
  const currentWord = words[currentWordIndex];
  const currentLetter = currentWord[currentLetterIndex];
  newTextElement.textContent += currentLetter;
  currentLetterIndex++;
  if (currentLetterIndex === currentWord.length) {
    isDeleting = true;
    setTimeout(() => {
      deleteLastLetter();
    }, 1000);
  } else {
    setTimeout(typeNextLetter, 100);
  }
}

function deleteLastLetter() {
  const currentWord = words[currentWordIndex];
  const currentLetter = currentWord[currentLetterIndex - 1];
  newTextElement.textContent = newTextElement.textContent.slice(0, -1);
  if (newTextElement.textContent.length === 0) {
    isDeleting = false;
    currentWordIndex = (currentWordIndex + 1) % words.length;
    currentLetterIndex = 0;
    setTimeout(typeNextLetter, 500);
  } else {
    setTimeout(deleteLastLetter, 50);
  }
}

setTimeout(() => {
  typeNextLetter();
}, 1000);

/*
document.getElementById("newText_small_device").textContent = "hotel, restaurace, rozhledna, nebo zajímavá turistická lokalita?";
*/

/*
***                                                     spoluprace SECTION 3
*/

const newTextElement2 = document.getElementById("newText2");
const words2 = ["Blogu", "Facebook", "Instagramu"];
let currentWordIndex2 = 0;
let currentLetterIndex2 = 0;
let isDeleting2 = false;

function typeNextLetter2() {
  const currentWord2 = words2[currentWordIndex2];
  const currentLetter2 = currentWord2[currentLetterIndex2];
  newTextElement2.textContent += currentLetter2;
  currentLetterIndex2++;
  if (currentLetterIndex2 === currentWord2.length) {
    isDeleting2 = true;
    setTimeout(() => {
      deleteLastLetter2();
    }, 1000);
  } else {
    setTimeout(typeNextLetter2, 100);
  }
}

function deleteLastLetter2() {
  const currentWord2 = words2[currentWordIndex2];
  const currentLetter2 = currentWord2[currentLetterIndex2 - 1];
  newTextElement2.textContent = newTextElement2.textContent.slice(0, -1);
  if (newTextElement2.textContent.length === 0) {
    isDeleting2 = false;
    currentWordIndex2 = (currentWordIndex2 + 1) % words2.length;
    currentLetterIndex2 = 0;
    setTimeout(typeNextLetter2, 500);
  } else {
    setTimeout(deleteLastLetter2, 50);
  }
}

setTimeout(() => {
  typeNextLetter2();
}, 1000);



$(".open_menu").click(function(){
  $(".navmenu").slideToggle()
})

/* slider2 aplikace*/
if ($(window).outerWidth() < 992) {
  $('.slide_celek2_img, .slide2_text').wrapAll('<div class="slide_img_text12"></div>');
}
$(document).ready(function() {
  if ($(window).outerWidth() < 992) {
    $('.slide_celek2_nadpis br').remove();
  }
});

/* slider3 aplikace*/
$(document).ready(function() {
  if ($(window).outerWidth() < 992) {
    $('.slide3_text_nadpis br').remove();
  }
});
$(document).ready(function() {
  if ($(window).outerWidth() < 992) {
    $('.slide4_textp br').remove();
  }
});

/*
****    spoluprace
*/



if (window.matchMedia("(max-width: 660px)").matches) {
  $(document).ready(function() {
    // Najdeme elementy s tridou "item_jste_text1_p" a "item_jste_img"
    var text = $('.item_jste_text1_p');
    var img = $('.item_jste_img');
    
    // Vlozime text pred obrazek
    img.before(text);
    
    // Vlozime obrazek pred text
    text.before(img);
  });
}



/* Toggle between showing and hiding the navigation menu links when the user clicks on the hamburger menu / bar icon */
/*function myFunction() {
  var x = document.getElementById("myLinks");
  if (x.style.display === "block") {
    x.style.display = "none";
  } else {
    x.style.display = "block";
  }
}
*/




































/*
$(document).ready(function() {
    // cache the jQuery object for the element with a class of header_background_foto
    var $headerBackground = $('.header_background_foto');
    
    // set the width of the element to the current width of the device
    $headerBackground.css('width', $(window).width() + 'px');
    
    // add an event listener to the window object for the 'resize' event
    $(window).on('resize', function() {
      // update the width of the element to the current width of the device when the window is resized
      $headerBackground.css('width', $(window).width() + 'px');
    });
  });

  container = document.getElementById("container");

var containerRect = container.getBoundingClientRect();

var topPadding = containerRect.top;
var bottomPadding = window.innerHeight - containerRect.bottom;
var leftPadding = containerRect.left;
var rightPadding = window.innerWidth - containerRect.right;

console.log("Top padding: " + topPadding + "px");
console.log("Bottom padding: " + bottomPadding + "px");
console.log("Left padding: " + leftPadding + "px");
console.log("Right padding: " + rightPadding + "px");

var leftPadding = containerRect.left;

var headerBackground = document.querySelector(".header_background_foto");
header_background_foto = "-" + leftPadding + "px";

/*
var headerBackground = document.querySelector(".container");
var headerBackgroundWidth = headerBackground.offsetWidth;
var deviceWidth = window.screen.widthvar || window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
console.log( + headerBackground + "px");
var width = headerBackground.offsetWidth;
console.log(typeof(headerBackground))
console.log( + deviceWidth + "px");

var leftPadding = width - deviceWidth;
console.log(leftPadding+ "px");
var leftPadding = leftPadding / 2;
headerBackground.style.marginLeft = leftPadding+ "px";

document.querySelector('.header_background_foto').style.marginLeft = (leftPadding+ "px");
*/














/*
$(window).on('load resize',function(){
  var padding = 15;
  var page_width = $(document).outerWidth();
  var container_width = $('header .container').outerWidth();
  var width_add = (page_width - container_width) / 2 + padding;
  $('.full_width').css('width','calc(100% + '+width_add+')');
})

function calcFunction() {
  var padding = 15;
  var page_width = $(document).outerWidth();
  var container_width = $('header .container').outerWidth();
  var width_add = (page_width - container_width) / 2 + padding;
  $('.full_width').css('width','calc(100% + '+width_add+')');
}

window.addEventListener("resize",calcFunction);
window.addEventListener("load",calcFunction);
*/
